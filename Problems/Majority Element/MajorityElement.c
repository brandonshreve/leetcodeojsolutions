#include <stdio.h>
#include <stdbool.h>

// Making use of the Moore Voting Algorithm
int majorityElement(int num[], int n) {
    int possibleMajority = num[0];
    int count = 1;
    
    for (uint16_t i = 1; i < n; i++) {
        if (count == 0) {
            possibleMajority = num[i];
            count = 1;
        }
        else if (possibleMajority == num[i]) {
            count++;
        }
        else {
            count--;
        }
        
    }
    return possibleMajority;
}
