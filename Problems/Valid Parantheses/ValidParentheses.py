__author__ = 'Brandon Shreve'

class ValidParentheses:
    # @return a boolean
    def isValid(self, s):
        openc = ['(', '{', '[']
        closec = [')', '}', ']']
        stack = []

        for c in s:
            if c in openc:
                stack.append(c)
            if c in closec:
                if len(stack) > 0:
                    if openc[closec.index(c)] == stack[-1]:
                        stack.pop()
                    else:
                        return False
                else:
                    return False

        return len(stack) == 0