#include <stdio.h>

void rotate(int nums[], int n, int k) {
    int newArray[n];
    
    for (uint i = 0; i < n; i++) {
        int newIndex = i+k;
        
        while (newIndex >= n)
            newIndex = newIndex-n;
        
        newArray[newIndex] = nums[i];
    }
    
    for (uint i = 0; i < n; i++)
        nums[i] = newArray[i];
}
