__author__ = 'brandonshreve'
import re


class ValidNumber:
    # @param s, a string
    # @return a boolean
    def isNumber(s):
        integerPattern = r'^-?[0-9]+$'
        floatPattern = r'^-?[0-9]*.[0-9]+$'
        expPattern = r'^[1-9]e[0-9]+$'

        # Remove any leading/trailing whitespace
        s = s.strip()

        patterns = [integerPattern, floatPattern, expPattern]

        for something in patterns:
            matched = re.match(something, s)

            if matched:
                if s in matched.group():
                    print("matched group: " + str(matched.group()))
                    return True
        return False